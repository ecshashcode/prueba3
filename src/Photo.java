import java.util.List;

public class Photo {
	
	private int id;
	private boolean orientation;
	private int tagNum;
	private List<String> tags;
	
	public Photo(int id, boolean orientation, int tagNum, List<String> tags) {
		super();
		this.id = id;
		this.orientation = orientation;
		this.tagNum = tagNum;
		this.tags = tags;
	}

	public Photo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isOrientation() {
		return orientation;
	}

	public void setOrientation(boolean orientation) {
		this.orientation = orientation;
	}

	public int getTagNum() {
		return tagNum;
	}

	public void setTagNum(int tagNum) {
		this.tagNum = tagNum;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public int compareTo(List<String> list) {
		int inner = 0;
		@SuppressWarnings("unused")
		int left = 0;
		int right = 0;
		
		for(String tmp: tags) {
			if(list.contains(tmp)) {
				inner++;
			}else {
				left++;
			}
		}
		right = list.size() - inner;
		int dev = 0;
		if(inner<=left) {
			dev = inner;
		}else{
			dev = left;
		}
		if(dev > right) {
			dev = right;
		}
		return dev;
		
	}
	
	
}
