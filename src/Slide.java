public class Slide {
	
	private int idPhoto;
	private boolean twoFotos;
	
	public Slide(int idPhoto, boolean twoFotos) {
		super();
		this.idPhoto = idPhoto;
		this.twoFotos = twoFotos;
	}
	
	public Slide() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdPhoto() {
		return idPhoto;
	}
	
	public void setIdPhoto(int idPhoto) {
		this.idPhoto = idPhoto;
	}
	
	public boolean isTwoFotos() {
		return twoFotos;
	}
	
	public void setTwoFotos(boolean twoFotos) {
		this.twoFotos = twoFotos;
	}	
} 