import java.util.List;

public class SlideShow {
	
	private int numbersPhoto;
	private int numberPhotoUsables;
	private List<Slide> listSlide;
	
	public SlideShow(int numbersPhoto, int numberPhotoUsables, List<Slide> listSlide) {
		super();
		this.numbersPhoto = numbersPhoto;
		this.numberPhotoUsables = numberPhotoUsables;
		this.listSlide = listSlide;
	}

	public SlideShow() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getNumbersPhoto() {
		return numbersPhoto;
	}

	public void setNumbersPhoto(int numbersPhoto) {
		this.numbersPhoto = numbersPhoto;
	}

	public int getNumberPhotoUsables() {
		return numberPhotoUsables;
	}

	public void setNumberPhotoUsables(int numberPhotoUsables) {
		this.numberPhotoUsables = numberPhotoUsables;
	}

	public List<Slide> getListSlide() {
		return listSlide;
	}

	public void setListSlide(List<Slide> listSlide) {
		this.listSlide = listSlide;
	}
	
	
} 